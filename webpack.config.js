'use strict'
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    mode: 'development',
    entry: [
        './src/app.js',
        './src/style/style.less'
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.less$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'less-loader'
                }]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    output: {
        // filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'public/dist'),
    },
}