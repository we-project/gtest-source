"use strict";

import Vue from 'vue';
import store from "./store";
import App from "./App.vue";

import functions from "./functions";
Vue.prototype.$func = functions;

new Vue({
    el: '#app',
    render: h => h(App),
    store,
    created(){
        //Обновляем данные во vuex из localstorage
        this.$store.commit('setDefaultData');

        //Проверяем есть ли тестовые данные
        if(!localStorage['data_orders_count']){
            this.$store.commit('setData', this.$func.randomData());
        }
    }
});