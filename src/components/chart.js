import { Bar } from 'vue-chartjs'

export default {
    extends: Bar,
    data: function () {
        return {
            options: {
                cornerRadius: 20,
                height: 300,
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    position: 'bottom'
                },
                tooltips: {
                    enabled: false,
                },
                scales: {
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero:true
                        }
                    }],
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        }
                    }],
                }

            }
        }
    },
    computed: {
        myStyles () {
            return {
                height: `${this.height}px`,
                width: `100%`,
                position: 'relative'
            }
        },
    },
    mounted () {
        // Overwriting base render method with actual data.
        this.renderChart({
            labels: ['Янв.', 'Фев.', 'Март', 'Апр.', 'Май', 'Июнь', 'Июль', 'Авг.', 'Сен.', 'Окт.', 'Ноя.', 'Дек.'],
            datasets: [
                {
                    backgroundColor: '#2F80ED',
                    data: [220, 380, 205, 290, 320, 190, 250, 170, 215, 295, 320, 320]
                },
                {
                    backgroundColor: '#F2C94C',
                    data: [310, 380, 205, 290, 320, 190, 250, 170, 215, 295, 320, 320]
                }
            ]
        }, this.options)
    }
}