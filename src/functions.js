"use strict";

export default {
    random(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    randomData(){
        return {
            orders: {
                count: this.random(1000, 5000),
                procent: this.random(-10, 10),
            },
            revenue: {
                count: this.random(1000000, 10000000),
                procent: this.random(-10, 10),
            },
            production: {
                count: this.random(1000, 5000),
                procent: this.random(-10, 10),
            }
        }
    }
}