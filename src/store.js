import Vue  from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        sidebar: {
            collapse: false,
            layout: 1,
            menu: 1,
        },
        data: {
            orders: {
                count: 0,
                procent: 0,
            },
            revenue: {
                count: 0,
                procent: 0,
            },
            production: {
                count: 0,
                procent: 0,
            }
        }
    },
    mutations: {
        setDefaultData(state, data){
            //Свернут ли сайдбар
            state.sidebar.collapse = (localStorage['collapse'] === 'true');
            //Выбранный layout
            state.sidebar.layout = (localStorage['layout'] ? localStorage['layout'] : 1);
            //Выбранная ссылка меню
            state.sidebar.menu = (localStorage['menu'] ? localStorage['menu'] : 1);

            //Подставляем данные
            state.data.orders.count = localStorage['data_orders_count'];
            state.data.orders.procent = localStorage['data_orders_procent'];
            state.data.revenue.count = localStorage['data_revenue_count'];
            state.data.revenue.procent = localStorage['data_revenue_procent'];
            state.data.production.count = localStorage['data_production_count'];
            state.data.production.procent = localStorage['data_production_procent'];
        },
        setData(state, data){
            //Подставляем данные
            state.data.orders.count = data.orders.count;
            localStorage['data_orders_count'] = data.orders.count;
            state.data.orders.procent = data.orders.procent;
            localStorage['data_orders_procent'] = data.orders.procent;
            state.data.revenue.count = data.revenue.count;
            localStorage['data_revenue_count'] = data.revenue.count;
            state.data.revenue.procent = data.revenue.procent;
            localStorage['data_revenue_procent'] = data.revenue.procent;
            state.data.production.count = data.production.count;
            localStorage['data_production_count'] = data.production.count;
            state.data.production.procent = data.production.procent;
            localStorage['data_production_procent'] = data.production.procent;
        },
        setSidebarLayout(state, layout){
            state.sidebar.layout = layout;
            localStorage['layout'] = layout;
        },
        setSidebarCollapse(state, collapse){
            state.sidebar.collapse = collapse;
            localStorage['collapse'] = collapse;
        },
        setSidebarMenu(state, menu){
            state.sidebar.menu   = menu;
            localStorage['menu'] = menu;
        },
    },
    getters: {
        sidebar: state => {
            return state.sidebar;
        },
        data: state => {
            return state.data;
        }
    }
});

export default store;